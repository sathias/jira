(defproject jira-cli "0.1.0-SNAPSHOT"
  :description "Jira cli written in clojure"
  :url "https://gitlab.com/sathias/jira"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"] [clj-http "3.10.0"] [cheshire "5.9.0"] [yaml "1.0.0"]]
  :main ^:skip-aot jira.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
