(ns jira.issue
  (:require [clj-http.client :as client])
  (:require [cheshire.core :as json])
  (:require [yaml.core :as yaml]))

(defn issue-config
    [key]
    (def config (yaml/from-file (str (System/getProperty "user.home") "/jira-bulk/config.yml")))
    (get-in config ["jira" key]))


(defn jira-url
  [path]
  (str (issue-config "host") "/rest/api/3/" path)
)

(defn parse-response
    [res]
      (json/parse-string (get res :body) true))

(defn custom-field-value-options
  []
  (issue-config "field_value_options"))

(defn credentials
  []
  {:basic-auth (str (issue-config "email_id") ":" (issue-config "api_key"))
    :headers {"Accept" "application/json", "Content-Type" "application/json"}
  })

(defn issues
  []
  (def res (client/get (str (jira-url "search?jql=") (issue-config "query"))
              (credentials)))
  (get (parse-response res) :issues))

(defn edit
  [key params]
  (def put-payload (merge {:body params} (credentials)))
  (def path (str (jira-url (str "/issue/" key))))
  (def res (client/put path put-payload))
  (:status res))

(defn summary
  [issue]
  [(get issue :key)  (get-in issue [:fields, :summary])]
)

(defn custom-field-value
  [json]
  (def custom-field (json/parse-string json true))
  (get-in custom-field [:fields, (keyword (issue-config "field_to_updated")), :value]))
