(ns jira.core
  (:require [jira.issue :as issue])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (def summary (map issue/summary (issue/issues)))
  (def options (issue/custom-field-value-options))
  (println (str "Total issues: " (count summary)))
  (doseq [[k v] options] (println (str k " - " (issue/custom-field-value v))))
  (doseq [[k v] summary]
    (println (str "\n\n" k " - " v))
    (def choice (read-line))
    (def opt (get options (read-string choice)))
    (println (issue/edit k opt))))
