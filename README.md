# Before running program
Make sure you follow below instructions.
`git clone git@gitlab.com:sathia.s/jira.git`
`cp config.yml.example ~/jira-bulk/config.yml`

# Get your credentials from jira.
Get your credentials from jira and update `~/jira-bulk/config.yml`

# Run as clojure
Inside project directory
`lein run`

# Generate uberjar
`lein uberjar`

# Run as jar (standalore)
`java -jar target/uberjar/jira-0.1.0-SNAPSHOT-standalone.jar`
